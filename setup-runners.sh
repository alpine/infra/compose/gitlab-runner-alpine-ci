#!/bin/sh

confirm() {
    prompt=$1
    printf "%s" "$prompt? [Yn] "
    read -n1 -s answer
    printf "\n"

    case $answer in
        n|N|no) return 1;;
        *) return 0;;
    esac
}

ask() {
    prompt=$1
    hide=${2-nohide}
    default=${3-}

    hide_arg=
    case $hide in
        ""|hide|hidden) hide_arg=-s;;
    esac

    printf "%s" "$prompt" >&2
    if [ -n "$default" ]; then
        printf " [%s]" "$default" >&2
    fi
    printf ": " >&2
    read -r ${hide_arg?} answer
    printf "\n" >&2

    if [ -n "$answer" ]; then
        echo "$answer"
    elif [ -n "$default" ]; then
        echo "$default"
    fi
}

enabled_profiles=$(ls -1 profiles-enabled)

if [ -z "$enabled_profiles" ]; then
    echo "No profiles have been enabled. You can enable profiles by symlinking them from"
    echo "profiles-available or adding custom profiles in profiles-enabled"
    exit 1
fi

echo "Enabled profiles:"
printf -- "- %s\n" $enabled_profiles
echo

if ! confirm "Do you want to register these runners"; then
    exit 0
fi

echo
echo "Please provide a personal access token used to create the runners. Ideally this"
echo "should be a short-lived token with just the create_runners scope."
echo 
echo "To create instance_type runners, a token from an administrator account is required"
echo "This token is not stored, but only used while registering the runners"
echo

token=$(ask "Token" hidden)
hostname=$(ask "Hostname" nohidden "$(hostname)")
arch=$(ask "Runner arch" nohidden "$(uname -m)")

docker compose run --rm \
    -e GITLAB_ACCESS_TOKEN="$token" \
    -e ALPINE_ARCH="$arch" \
    -e RUNNER_HOSTNAME="$hostname" \
    gitlab-runner --no-start

echo
echo "-> Finished. Run 'docker compose up -d' to start the runners"
