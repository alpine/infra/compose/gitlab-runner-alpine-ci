# gitlab-runner

docker-compose files for running a gitlab-runner used for running Alpine CI/CD
jobs

## Quick Setup

Make sure you have a personal access token available with sufficient
permissions. It needs at least the `create runners` scope. To create shared
runners, the token must belong to an administrator.

Enable one or more profiles by placing them in `profiles-enabled` and then run
`setup-runners.sh` and answer the questions.

Then start the runner with: `docker compose up -d`

